#include <stdio.h>
#include "test/test.h"
#include "main/util.h"
int main(void) {
    const char* file_name = "test.txt";
    FILE* fl = fopen(file_name, "w");
    for (size_t i = 0; i < 6; i++) {
        tests[i](fl);
    }
    printf("Tests ok. \nResults: /build/%s\n  ", file_name);
    return 0;
}
