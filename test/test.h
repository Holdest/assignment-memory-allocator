#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_TEST_H
#include <stdio.h>
typedef void (test)(FILE* output);

void test_alloc(FILE* output);

void free_block(FILE* output);

void free_two_blocks(FILE* output);

void merging(FILE* output);

void extending(FILE* output);

void cant_extending(FILE* output);

static test* const tests[] = {
        test_alloc,
        free_block,
        free_two_blocks,
        merging,
        extending,
        cant_extending
};
#endif //ASSIGNMENT_MEMORY_ALLOCATOR_MASTER_TEST_H
