#include "test.h"
#include <stdio.h>
#include <assert.h>
#include <sys/mman.h>
#include <inttypes.h>
#include "../main/mem.h"
#include "../main/mem_internals.h"

#define DEF_SIZE 1024
#define HEAP_SIZE (1024 * 1024)

struct test_block {
    block_size sz;
    block_capacity cap;
    struct block_header* block;
};

static const struct test_block empty_block = { 0 };

static void print_test_start(const char* test_name, FILE* output) {
    fprintf(output, "test: %s \n", test_name);
    fflush(output);
}

static void print_test_end(FILE* output) {
    fprintf(output, "ok\n");
    fflush(output);
}

static int clear_heap(void* heap, size_t heap_size) {
    return munmap(heap, heap_size);
}

static struct test_block alloc_block(size_t query) {
    void* block_content = _malloc(query);
    if (!block_content) return empty_block;
    struct block_header* block_header = block_get_header(block_content);
    block_capacity capacity = block_header->capacity;
    return (struct test_block) {
            .cap = capacity,
            .sz = size_from_capacity(capacity),
            .block = block_header
    };
}

static void print_debug_heap(FILE* output, void* heap_start) {
    debug_heap(output, heap_start);
    fflush(output);
}

static void* test_start(const char* test_name, FILE* output) {
    print_test_start(test_name, output);
    void* heap_start = heap_init(HEAP_SIZE);
    assert(heap_start != NULL);
    return heap_start;
}

static void test_end(FILE* output, void* heap_start) {
    clear_heap(heap_start, HEAP_SIZE);
    print_test_end(output);
}

void test_alloc(FILE* output) {
    void* heap_start = test_start("allocation", output);
    size_t first_block_cap = DEF_SIZE;
    struct test_block first_block = alloc_block(first_block_cap);
    print_debug_heap(output, heap_start);
    assert((void*) first_block.block == heap_start && first_block.cap.bytes == first_block_cap);
    size_t second_block_cap = 5 * DEF_SIZE;
    struct test_block second_block = alloc_block(second_block_cap);
    print_debug_heap(output, heap_start);
    assert(second_block.block == first_block.block->next);
    assert((uint8_t*) first_block.block + first_block.sz.bytes == (uint8_t*) second_block.block);
    block_size free_block_sz = {HEAP_SIZE - first_block.sz.bytes - second_block.sz.bytes };
    assert(second_block.block->next->capacity.bytes == capacity_from_size(free_block_sz).bytes);

    test_end(output, heap_start);
}

void free_block(FILE* output) {
    void* heap_start = test_start("free  block", output);
    size_t first_block_cap = DEF_SIZE;
    struct test_block first_block = alloc_block(first_block_cap);
    size_t second_block_cap = 5 * DEF_SIZE;
    struct test_block second_block = alloc_block(second_block_cap);
    print_debug_heap(output, heap_start);
    _free(second_block.block->contents);
    print_debug_heap(output, heap_start);
    const struct block_header* free_block = first_block.block->next;
    assert(free_block->is_free && free_block->next == NULL);
    assert(size_from_capacity(free_block->capacity).bytes == HEAP_SIZE - first_block.sz.bytes);

    test_end(output, heap_start);
}

void free_two_blocks(FILE* output) {
    void* heap_start = test_start("free two blocks", output);
    size_t first_block_capacity = DEF_SIZE;
    struct test_block first_block = alloc_block(first_block_capacity);
    size_t second_block_capacity = 5 * DEF_SIZE;
    struct test_block second_block = alloc_block(second_block_capacity);
    size_t third_block_capacity = 1000 * DEF_SIZE;
    struct test_block third_block = alloc_block(third_block_capacity);
    print_debug_heap(output, heap_start);
    _free(second_block.block->contents);
    _free(third_block.block->contents);
    print_debug_heap(output, heap_start);
    struct block_header* first_free_block = first_block.block->next;
    struct block_header* second_free_block = first_free_block->next;
    assert(first_free_block->is_free);
    assert(first_free_block->capacity.bytes == second_block_capacity);
    assert(second_free_block->is_free && second_free_block->next == NULL);
    test_end(output, heap_start);
}

void merging(FILE* output) {
    void* heap_start = test_start("merging", output);
    size_t first_block_capacity= DEF_SIZE;
    struct test_block first_block = alloc_block(first_block_capacity);
    size_t second_block_capacity = 5 * DEF_SIZE;
    struct test_block second_block = alloc_block(second_block_capacity);
    size_t third_block_capacity = 1000 * DEF_SIZE;
    struct test_block third_block = alloc_block(third_block_capacity);
    _free(second_block.block->contents);
    _free(third_block.block->contents);
    print_debug_heap(output, heap_start);
    size_t fourth_block_capacity = 10 * DEF_SIZE;
    struct test_block fourth_block = alloc_block(fourth_block_capacity);
    print_debug_heap(output, heap_start);
    struct block_header* free_block = fourth_block.block->next;
    assert(fourth_block.block && free_block->next == NULL);
    assert(size_from_capacity(free_block->capacity).bytes == HEAP_SIZE - first_block.sz.bytes - fourth_block.sz.bytes);
    test_end(output, heap_start);
}

void extending(FILE* output) {
    void* heap_start = test_start("extending", output);
    size_t first_block_capacity = HEAP_SIZE - 32;
    struct test_block first_block = alloc_block(first_block_capacity);
    size_t second_block_capacity = DEF_SIZE;
    struct test_block second_block = alloc_block(second_block_capacity);
    print_debug_heap(output, heap_start);
    assert(second_block.block == first_block.block->next);
    assert((uint8_t*) first_block.block + first_block.sz.bytes == (uint8_t*) second_block.block);
    assert(first_block.block->capacity.bytes + second_block.block->capacity.bytes > HEAP_SIZE);
    test_end(output, heap_start);
}

void cant_extending(FILE* output) {
    void* heap_start = test_start("cant extending", output);
    size_t first_block_capacity = HEAP_SIZE - 32;
    struct test_block first_block = alloc_block(first_block_capacity);
    size_t second_block_capacity = DEF_SIZE * DEF_SIZE * DEF_SIZE;
    struct test_block second_block = alloc_block(second_block_capacity);
    print_debug_heap(output, heap_start);
    assert(second_block.block == first_block.block->next);
    assert((uint8_t*) first_block.block + first_block.sz.bytes != (uint8_t*) second_block.block);

    test_end(output, heap_start);
}
