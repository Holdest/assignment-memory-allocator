#define _DEFAULT_SOURCE
#include <unistd.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <stddef.h>
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#define HEAP_START ((void*)0x00010000)
#define BLOCK_MIN_CAPACITY 24
#define REGION_MIN_SIZE (2 * 4096)

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK,
        BSR_REACHED_END_NOT_FOUND,
    } type;
    struct block_header* block;
};

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);
extern inline bool region_is_invalid(const struct region* const r);

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init( void* restrict address, block_size block_sz, void* restrict next ) {
    *((struct block_header*)address) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}
static size_t region_actual_size(size_t query) { return size_max(round_pages( query ), REGION_MIN_SIZE); }

static void* map_pages(const void* const address, const size_t length, const int additional_flags) {
    return mmap((void*) address, length,PROT_READ | PROT_WRITE,MAP_PRIVATE | MAP_ANONYMOUS | additional_flags,0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(const void* address, size_t query, int additional_flags) {
    query = region_actual_size(query);
    void* region_address = map_pages(address, query, additional_flags);
    if (region_address == MAP_FAILED)
        region_address = alloc_region(0, query, 0).address;
    else {
        block_init(region_address, (block_size) { query }, NULL);
    }
    struct region region = {
            .address = region_address,
            .size = query,
            .extends = address == region_address
    };
    return region;
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */
static bool block_is_splittable(const size_t query, const struct block_header* const restrict block) {
    const block_size min_block_size = size_from_capacity((block_capacity) { BLOCK_MIN_CAPACITY });
    return block->is_free && query + min_block_size.bytes <= block->capacity.bytes;
}

static bool try_split_block(const size_t query, struct block_header* const block) {
    if (block_is_splittable(query, block)) {
        void* const next_block_address = (void*) (block->contents + query);
        const block_size next_block_sz = { block->capacity.bytes - query };
        block_init(next_block_address, next_block_sz, block->next);
        block->next = next_block_address;
        block->capacity = (block_capacity) { query };
        return true;
    } else return false;
}


/*  --- Слияние соседних свободных блоков --- */
static void* block_after(const struct block_header* const block) {
    return (void*) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(const struct block_header* const fst,
                              const struct block_header* const snd) {
    return (void*) snd == block_after(fst);
}

static bool mergeable(const struct block_header* const restrict fst, const struct block_header* const restrict snd) {
    return fst && snd && fst->is_free && snd->is_free && blocks_continuous(fst, snd) ;
}

static bool merge_next(struct block_header* const block) {
    if (mergeable(block, block->next)) {
        block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
        block->next = block->next->next;
        return true;
    } else
        return false;
}

/*  --- ... ecли размера кучи хватает --- */
static struct block_search_result bsr_init_bad_result() {
    return (struct block_search_result) { .type = BSR_REACHED_END_NOT_FOUND, .block = NULL };
}

static struct block_search_result bsr_init_good_result(struct block_header* found_block) {
    return (struct block_search_result) { .type = BSR_FOUND_GOOD_BLOCK, .block = found_block };
}

static bool is_good_search_result(const struct block_search_result search_result) {
    return search_result.type == BSR_FOUND_GOOD_BLOCK;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static bool block_is_big_enough(const size_t query, const struct block_header* const block) {
    return block->capacity.bytes >= query;
}

static struct block_search_result try_memalloc_existing(const size_t query, struct block_header* block) {
    struct block_search_result result = bsr_init_bad_result();
    while (result.type != BSR_FOUND_GOOD_BLOCK && block) {
        if (block->is_free && block_is_big_enough(query, block)) {
            try_split_block(query, block);
            block->is_free = false;
            result = bsr_init_good_result(block);
        } else if (!merge_next(block)) {
            result.block = block;
            block = block->next;
        }
    }
    return result;
}


static struct block_header* grow_heap(const size_t query, struct block_header* const restrict last) {
    const void* address = (void*) (last->contents + last->capacity.bytes);
    const struct region region = alloc_region(address, query, 0);
    if (region_is_invalid(&region))
        return NULL;
    last->next = region.address;
    if (merge_next(last))
        return last;
    else
        return region.address;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(const size_t query, struct block_header* const heap_start) {
    const struct block_search_result  search_result = try_memalloc_existing(query, heap_start);
    if (is_good_search_result(search_result))
        return search_result.block;
    const block_size query_with_block_header = size_from_capacity((block_capacity) { query });
    struct block_header* const new_heap_start = grow_heap(query_with_block_header.bytes, search_result.block);
    if (!new_heap_start)
        return NULL;
    else
        return memalloc(query, new_heap_start);
}

void* heap_init(const size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial, MAP_FIXED);
    if (region_is_invalid(&region) || region.address != HEAP_START) return NULL;
    return region.address;
}

void* _malloc(const size_t query) {
    struct block_header* const address = memalloc(query, (struct block_header*) HEAP_START);
    if (address) return address->contents;
    else return NULL;
}

struct block_header* block_get_header(const void* const contents) {
    return (struct block_header*) (((uint8_t*) contents) - offsetof(struct block_header, contents));
}

void _free(void* mem) {
    if (!mem) return;
    struct block_header* header = block_get_header(mem);
    header->is_free = true;
    merge_next(header);
}

